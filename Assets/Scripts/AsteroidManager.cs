﻿using System.Linq;
using UnityEngine;
using Quaternion = UnityEngine.Quaternion;
using Vector2 = UnityEngine.Vector2;
using Vector3 = UnityEngine.Vector3;

public class AsteroidManager : MonoBehaviour
{    
    private Vector3 _tmpPos;

    private GameObject[] _respawns;


    private Vector2 _siz;

    private int _nbFoes = 10; // number of foes spawning

    
    void Update()
    {

        // Create a tab containing all the asteroid in a scene
        _respawns = GameObject.FindGameObjectsWithTag("asteroid");
        // If less than 10 asteroids …
        if (_respawns.Length > 0)
        {
            _siz.x = _respawns[0].GetComponent<SpriteRenderer>().bounds.size.x;
            _siz.y = _respawns[0].GetComponent<SpriteRenderer>().bounds.size.y;
        }

        // score between 20 and 40
        if (GameState.Instance.ScorePlayer > 20 && GameState.Instance.ScorePlayer < 40)
            _nbFoes = 10;
        // score between 40 and 80
        else if (GameState.Instance.ScorePlayer > 40 && GameState.Instance.ScorePlayer < 80)
        {
            _nbFoes = 15;
        }
        // score superior 80
        else if (GameState.Instance.ScorePlayer > 100)
        {
            _nbFoes = 20;
        }
        
        if (_respawns.Length < _nbFoes)
        {
            // Choose randomly if an asteroid will be created or not
            if (Random.Range(1, 1000) < 50 || _respawns.Length < 4)
            {
                // Create a new asteroid
                Vector3 tmpPos = new Vector3(GameState.Instance.RightBottomCameraBorder.x + (_siz.x / 2),
                    Random.Range(GameState.Instance.RightBottomCameraBorder.y + (_siz.y / 2),
                        (GameState.Instance.RightTopCameraBorder.y - (_siz.y / 2))),
                    transform.position.z);
                // Instanciation
                GameObject gY = Instantiate(Resources.Load("asteroidSP"), tmpPos, Quaternion.identity) as GameObject;
            }
        }
    }
}
