﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;
using UnityEngine.SceneManagement;
public class ChangeWeapon: MonoBehaviour
{

    private float cd = 2;
    private float _lastShoot;

    private void Update()
    {
        bool keyX = Input.GetKeyDown(KeyCode.X);
        if (keyX && _lastShoot < Time.time)
        {
            if (gameObject.GetComponent<TripleShoot>())
            {
                Destroy(gameObject.GetComponent<TripleShoot>());
                gameObject.AddComponent<TripleShootStraight>();
                SoundState.Instance.SwitchSound();

            }
            else if (gameObject.GetComponent<TripleShootStraight>())
            {
                Destroy(gameObject.GetComponent<TripleShootStraight>());
                gameObject.AddComponent<TripleShoot>();
                SoundState.Instance.SwitchSound();
            }

            _lastShoot = Time.time + cd;
        }
    }
}
