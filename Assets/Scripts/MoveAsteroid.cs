﻿using UnityEngine;
using Random = UnityEngine.Random;

public class MoveAsteroid : MonoBehaviour
{
    public Vector2 _movement;

    private Vector3 _sprite;
    private Transform _asteroid;
    
    
    // Start is called before the first frame update
    private void Start()
    {
        _asteroid = transform;
        
        float y = Random.Range(GameState.Instance.LeftBottomCameraBorder.y, GameState.Instance.RightTopCameraBorder.y);

            // set initial pos of the asteroid 
        _asteroid.position = new Vector3(GameState.Instance.RightTopCameraBorder.x,
                            y,
                            _asteroid.position.z);
        
        
        // get the size of the Sprite
        _sprite.x = gameObject.GetComponent<SpriteRenderer>().bounds.size.x;
        _sprite.y = gameObject.GetComponent<SpriteRenderer>().bounds.size.y;
        _sprite.z = gameObject.GetComponent<SpriteRenderer>().bounds.size.z;


        float moveY = Random.Range(-1f, 1f);
        // set movement
        _movement = new Vector2(
            Random.Range(-10.0f, -8.0f),
            moveY);
        // Random.Range(-0.5f, 0.5f));

        GetComponent<Rigidbody2D>().velocity = _movement;
    }

    // Update is called once per frame
    private void Update()
    {
        // destroy when out of window
        if (_asteroid.position.x < GameState.Instance.LeftTopCameraBorder.x - _sprite.x / 2 || 
            _asteroid.position.y > GameState.Instance.LeftTopCameraBorder.y + _sprite.y / 2 ||  
            _asteroid.position.y < GameState.Instance.LeftBottomCameraBorder.y - _sprite.y / 2)
        {    
            // destroy asteroid
            Destroy(gameObject);
        }
    }
}
