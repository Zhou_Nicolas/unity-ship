﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour
{
    private Vector3 _tmpPos;

    private GameObject[] _respawns;


    private Vector2 _siz;

    private int _nbFoes = 0; // number of foes spawning

    private int level = 1;
    
    void Update()
    {

        // Create a tab containing all the asteroid in a scene
        _respawns = GameObject.FindGameObjectsWithTag("enemy");
        // If less than 10 asteroids …
        if (_respawns.Length > 0)
        {
            _siz.x = _respawns[0].GetComponent<SpriteRenderer>().bounds.size.x;
            _siz.y = _respawns[0].GetComponent<SpriteRenderer>().bounds.size.y;
        }

        // score between 20 and 40
        if (level == 1 && GameState.Instance.ScorePlayer > 20 )
        {
            _nbFoes = 1;
            level += 1;
        }
        // score between 40 and 80
        else if (level == 2 && GameState.Instance.ScorePlayer > 40)
        {
            _nbFoes = 3;
            level += 1;
        }
        // score superior 100
        else if (level == 3 && GameState.Instance.ScorePlayer > 100)
        {
            _nbFoes = 5;
            level += 1;
        }
        // score > 200 then decrease cd shoot 
        else if (level == 4 && GameState.Instance.ScorePlayer > 200)
        {
            _nbFoes = 10;
            GameState.Instance.EnemyShootCd /= 2;
            SoundState.Instance.WarningSound();
            level += 1;
        }
        // score > 400 then decrease more cd shoot 
        else if (level == 5 && GameState.Instance.ScorePlayer > 400)
        {
            GameState.Instance.EnemyShootCd /= 2;
            SoundState.Instance.WarningSound();
            level += 1;
        }
        
        if (_respawns.Length < _nbFoes)
        {
            // Choose randomly if an asteroid will be created or not
            if (Random.Range(1, 100) < 50 || _respawns.Length < 4)
            {
                // Create a new asteroid
                Vector3 tmpPos = new Vector3(GameState.Instance.RightBottomCameraBorder.x + (_siz.x / 2),
                    Random.Range(GameState.Instance.RightBottomCameraBorder.y + (_siz.y / 2),
                        (GameState.Instance.RightTopCameraBorder.y - (_siz.y / 2))),
                    transform.position.z);
                // Instanciation
                GameObject gY = Instantiate(Resources.Load("enemySP"), tmpPos, Quaternion.identity) as GameObject;
            }
        }
    }
}
