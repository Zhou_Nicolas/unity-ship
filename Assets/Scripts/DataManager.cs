﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class DataManager : MonoBehaviour
{
    private static DataManager _instance = null;

    private int currentScore;

    private List<int> ranking;

    public string username = "unknown";
    public static DataManager Instance {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<DataManager>();
                if (_instance == null)
                {
                    GameObject go = new GameObject();
                    go.name = "DataManager";
                    _instance = go.AddComponent<DataManager>();
                    DontDestroyOnLoad(go);
                }
            }

            return _instance;
        }
    }

    public void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
            ranking = new List<int>();
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void setCurrentScore(int val)
    {
        currentScore = val;
        if (ranking.Count == 0)
        {
            ranking.Add(val);
        }
        else
        {
            bool added = false;
            for (int index=0; index < ranking.Count; index++ )
            {
                if (val > ranking.ElementAt(index))
                {
                    ranking.Insert(index, val);
                    added = true;
                    break;
                } 
            }

            if (!added && ranking.Count < 5)
            {
                ranking.Add(val);
            }
            else if (ranking.Count > 5)
            {
                ranking.RemoveAt(-1);
            }
        }

    }

    public int getCurrentScore()
    {
        return currentScore;
    }

    public void updateRanking()
    {
        for (int i = 0; i < ranking.Count; i++)
        {
            GameObject.FindWithTag("score"+(i+1)).transform.Find("Score").GetComponent<Text>().text = "" + ranking.ElementAt(i);
            GameObject.FindWithTag("score"+(i+1)).transform.Find("Name").GetComponent<Text>().text = username;

        }
    }

    public string Username
    {
        get => username;
        set => username = value;
    }
    
}
