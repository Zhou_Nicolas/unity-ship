﻿

using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;

public class MoveShootUp : MonoBehaviour
{
    public Vector2 _movement;
    
    private Vector3 _siz;

    private Transform _shoot;
    
    // Start is called before the first frame update
    void Start()
    {
        _shoot = transform;

        // get the size of the Sprite
        _siz.x = gameObject.GetComponent<SpriteRenderer>().bounds.size.x;
        _siz.y = gameObject.GetComponent<SpriteRenderer>().bounds.size.y;

        
        _movement = new Vector2(5.0f, 1f);

        GetComponent<Rigidbody2D>().velocity = _movement;
    }

    // Update is called once per frame
    void Update()
    {
        // delete when out of window
        if (_shoot.position.x > GameState.Instance.RightTopCameraBorder.x + _siz.x / 2)
        {
            Destroy(gameObject);
        }
    }
    
    void OnTriggerEnter2D(Collider2D collider) {
        // Add the fade script to the gameObject containing this script
        if (collider.name.Contains("SP"))
        {
            SoundState.Instance.AsteroidDestroySound();
            Destroy(collider.gameObject);
            // Add score 
            GameState.Instance.ScorePlayer += 1;
            if (Random.Range(0f, GameState.Instance.RandPowerup) < 1)
            {
                Vector3 tmpPos = new Vector3 (transform.position.x + _siz.x,
                    transform.position.y,
                    transform.position.z);
                GameObject g2 = Instantiate (Resources.Load ("powerup"), tmpPos, Quaternion.identity) as GameObject;
            }
            // Shoot destroy
            Destroy (gameObject);
        }
    }
}
