﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class ClickButton: MonoBehaviour
{
    public void clickStart()
    {
        SceneManager.LoadScene("Game");
    }

    public void clickRetry()
    {
        SceneManager.LoadScene("Game");
        
    }

    public void clickMenu()
    {
        SceneManager.LoadScene("Menu");
    }

    public void clickRanking()
    {
        SceneManager.LoadScene("Ranking");
    }

    public void clickInstructions()
    {
        SceneManager.LoadScene("Instructions");
    }
    
    public void clickGitLab()
    {
        Application.OpenURL("https://gitlab.com/Zhou_Nicolas/unity-ship");
    }

}
