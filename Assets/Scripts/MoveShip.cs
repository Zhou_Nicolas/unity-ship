﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class MoveShip : MonoBehaviour
{
    // La vitesse de deplacement
    public Vector2 speed;

    // Stockage du movement
    public Vector2 movement;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.name.Contains("asteroidSP") || other.name.Contains("enemyShoot"))
        {   
            // ship get damaged
            if (!gameObject.GetComponent<Damaged>())
            {

                gameObject.AddComponent<Damaged>();
                // lose life
                if (GameObject.FindGameObjectWithTag("life5"))
                    GameObject.FindGameObjectWithTag("life5").AddComponent<FadeOut>();
                else if (GameObject.FindGameObjectWithTag("life4"))
                    GameObject.FindGameObjectWithTag("life4").AddComponent<FadeOut>();
                else if (GameObject.FindGameObjectWithTag("life3"))
                    GameObject.FindGameObjectWithTag("life3").AddComponent<FadeOut>();
                else if (GameObject.FindGameObjectWithTag("life2"))
                    GameObject.FindGameObjectWithTag("life2").AddComponent<FadeOut>();
                else if (GameObject.FindGameObjectWithTag("life1"))
                {
                    GameObject.FindGameObjectWithTag("life1").AddComponent<FadeOut>();
                    DataManager.Instance.setCurrentScore(GameState.Instance.ScorePlayer);
                    Destroy(gameObject);
                    SceneManager.LoadScene("GameOver");
                }

                SoundState.Instance.DamagedSound();
                // Destroy the asteroid
                Destroy(other.gameObject);
            }
        }
        else if (other.name.Contains("powerup"))
        {
            if (gameObject.GetComponent<ShootAgain>())
            {
                Destroy(gameObject.GetComponent<ShootAgain>());
                gameObject.AddComponent<TripleShoot>();
            }
            else
            {
                GameState.Instance.TripleShootCd1 /= 2;
            }
            Destroy(other.gameObject);
            SoundState.Instance.PowerUpSound();
            GameState.Instance.RandPowerup *= 2;
        }


    }

    // Update is called once per frame
    void Update()
    {
        // recup infos inputs
        float inputY = Input.GetAxis("Vertical");
        float inputX = Input.GetAxis("Horizontal");
        // calcul movement
        movement = new Vector2(
        speed.x * inputX,
        speed.y * inputY);

        GetComponent<Rigidbody2D>().velocity = movement;
    }

    void FixedUpdate(){

    }
}
