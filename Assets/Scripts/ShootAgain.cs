﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootAgain : MonoBehaviour
{
    private Vector2 _siz;

    private float _lastShoot;

    void Update () {
        // Get the size of the gameObject containing this script
        _siz.x = gameObject.GetComponent<SpriteRenderer> ().bounds.size.x;
        _siz.y = gameObject.GetComponent<SpriteRenderer> ().bounds.size.y;
        // If space key pressed
        bool sp = Input.GetKey(KeyCode.Space);
        if (sp && _lastShoot < Time.time) {
            // Get the posi'on of the shoot using the ship posi'on
            Vector3 tmpPos = new Vector3 (transform.position.x + _siz.x,
            transform.position.y,
            transform.position.z);
            // Instantiate shootOrange
            GameObject gY = Instantiate (Resources.Load ("shootOrange"), tmpPos, Quaternion.identity) as GameObject;
            SoundState.Instance.TouchButtonSound(); // make shoot sound

            _lastShoot = Time.time + GameState.Instance.ShootCd1; // set new cooldown
        }
    }
}
