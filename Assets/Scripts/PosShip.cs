﻿using UnityEngine;

public class PosShip : MonoBehaviour
{
    private Vector3 _siz;

    private Transform _ship;

    private void Start()
    {
        _ship = transform;
        // get the size of the Sprite
        _siz.x = gameObject.GetComponent<SpriteRenderer>().bounds.size.x;
        _siz.y = gameObject.GetComponent<SpriteRenderer>().bounds.size.y;

        // set default pos to left center
        _ship.position = new Vector3(GameState.Instance.LeftTopCameraBorder.x + _siz.x / 2, 
            GameState.Instance.LeftTopCameraBorder.y + GameState.Instance.LeftBottomCameraBorder.y, 
                                       _ship.position.z);
    }

    // Update is called once per frame
    void Update()
    {
        if (_ship.position.x < GameState.Instance.LeftTopCameraBorder.x + _siz.x / 2)
        {
            _ship.position = new Vector3(GameState.Instance.LeftTopCameraBorder.x + _siz.x / 2, _ship.position.y, _ship.position.z);
        }
        else if (_ship.position.x > GameState.Instance.RightTopCameraBorder.x - _siz.x / 2)
        {
            _ship.position = new Vector3(GameState.Instance.RightTopCameraBorder.x - _siz.x / 2, _ship.position.y, _ship.position.z);
        }
        else if (_ship.position.y > GameState.Instance.LeftTopCameraBorder.y - _siz.y / 2)
        {
            _ship.position = new Vector3(_ship.position.x, GameState.Instance.LeftTopCameraBorder.y - _siz.y / 2, _ship.position.z);
        }
        else if ( _ship.position.y < GameState.Instance.LeftBottomCameraBorder.y + _siz.y / 2)
        {
            _ship.position = new Vector3(_ship.position.x, GameState.Instance.LeftBottomCameraBorder.y + _siz.y / 2, _ship.position.z);
        }
    }
}
