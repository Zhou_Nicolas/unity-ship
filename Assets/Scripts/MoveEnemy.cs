﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveEnemy : MonoBehaviour
{
    public Vector2 _movement;

    private Vector3 _sprite;
    private Transform _enemy;

    private float _lastShoot;

    
    // Start is called before the first frame update
    private void Start()
    {
        _enemy = transform;
        
        float y = Random.Range(GameState.Instance.LeftBottomCameraBorder.y, GameState.Instance.RightTopCameraBorder.y);

        // set initial pos of the asteroid 
        _enemy.position = new Vector3(GameState.Instance.RightTopCameraBorder.x,
            y,
            _enemy.position.z);
        
        
        // get the size of the Sprite
        _sprite.x = gameObject.GetComponent<SpriteRenderer>().bounds.size.x;
        _sprite.y = gameObject.GetComponent<SpriteRenderer>().bounds.size.y;
        _sprite.z = gameObject.GetComponent<SpriteRenderer>().bounds.size.z;


        float moveY = Random.Range(-1f, 1f);
        // set movement
        _movement = new Vector2(
            Random.Range(-4.0f, -1.0f),
            moveY);
        // Random.Range(-0.5f, 0.5f));

        GetComponent<Rigidbody2D>().velocity = _movement;
    }

    // Update is called once per frame
    private void Update()
    {
        // auto shoot 
        if (_lastShoot < Time.time)
        {
           
            Vector3 tmpPos = new Vector3 (transform.position.x - _sprite.x,
                transform.position.y,
                transform.position.z);
            // Instantiate shootOrange
            GameObject gY = Instantiate (Resources.Load ("enemyShoot"), tmpPos, Quaternion.identity) as GameObject;
            SoundState.Instance.TouchButtonSound(); // make shoot sound

            _lastShoot = Time.time + GameState.Instance.EnemyShootCd; // set new cooldown
        }
        
        // destroy when out of window
        if (_enemy.position.x < GameState.Instance.LeftTopCameraBorder.x - _sprite.x / 2 || 
            _enemy.position.y > GameState.Instance.LeftTopCameraBorder.y + _sprite.y / 2 ||  
            _enemy.position.y < GameState.Instance.LeftBottomCameraBorder.y - _sprite.y / 2)
        {    
            // destroy asteroid
            Destroy(gameObject);
        }
    }
}
