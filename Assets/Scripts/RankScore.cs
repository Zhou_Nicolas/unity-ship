﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RankScore : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        SoundState.Instance.GameoverSound();
        GameObject.FindWithTag("finalScore").GetComponent<Text>().text += "" + DataManager.Instance.getCurrentScore();
   
    }

}
