﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moveBg : MonoBehaviour
{
    public Vector2 movement;
    
    private Vector3 siz;

    private Vector3 _leftBottomCameraBorder;
    private Vector3 _rightBottomCameraBorder;

    public float positionRestartX;

    
    // Start is called before the first frame update
    void Start()
    {
        
        if (!(Camera.main is null))
        {
            _leftBottomCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, 0));
            _rightBottomCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, 0));

        }
        // x of bg4
        GetComponent<Rigidbody2D>().velocity = movement;
        siz.x = gameObject.GetComponent<SpriteRenderer>().bounds.size.x;
        siz.y = gameObject.GetComponent<SpriteRenderer>().bounds.size.y;
        positionRestartX = _rightBottomCameraBorder.x - _leftBottomCameraBorder.x; // there are 3 bg so we need to put in pos of the third one
                                         // it's equal to 2 times width of the bg minus half for the start position
    
    }

    // Update is called once per frame
    void Update()
    {
        GetComponent<Rigidbody2D>().velocity = movement;
        siz.x = gameObject.GetComponent<SpriteRenderer>().bounds.size.x;
        siz.y = gameObject.GetComponent<SpriteRenderer>().bounds.size.y;
        if (transform.position.x < _leftBottomCameraBorder.x - siz.x / 2)
        {
            transform.position = new Vector3(positionRestartX, transform.position.y, transform.position.z);
        }

        
    }
}
