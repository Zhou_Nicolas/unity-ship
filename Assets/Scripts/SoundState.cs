﻿using System.Collections;
using UnityEngine;

public class SoundState : MonoBehaviour
{
    private static SoundState _instance = null;

    public AudioClip playerShotSound;
    public AudioClip asteroidDestroySound;
    public AudioClip warningSound;
    public AudioClip damagedSound;
    public AudioClip gameoverSound;
    public AudioClip powerupSound;
    public AudioClip switchSound;

    public AudioSource audioBGM;
    public static SoundState Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<SoundState>();
                if (_instance == null)
                {
                    GameObject go = new GameObject();
                    go.name = "SoundState";
                    _instance = go.AddComponent<SoundState>();
                    DontDestroyOnLoad(go);
                }
            }
            return _instance;
        }
    }

    public void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
            audioBGM = GetComponent<AudioSource>();
            playBGM();
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);   
        }


    }
    
    public void TouchButtonSound()
    {
        MakeSound(playerShotSound);
    }

    public void AsteroidDestroySound()
    {
        MakeSound(asteroidDestroySound);        
    }

    public void WarningSound()
    {
        MakeSound(warningSound);
    }
    public void DamagedSound()
    {
        MakeSound(damagedSound);
    }

    public void GameoverSound()
    {
        MakeSound(gameoverSound);
    }
    
    public void PowerUpSound()
    {
        MakeSound(powerupSound);
    }

    public void SwitchSound()
    {
        MakeSound(switchSound);
    }
    
    public void playBGM()
    {    
        if (!audioBGM.isPlaying)
            audioBGM.Play();
    }

    private void MakeSound(AudioClip originalClip)
    {
        AudioSource.PlayClipAtPoint(originalClip, transform.position);
    }
}
