﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShoot : MonoBehaviour
{
    public Vector2 _movement;
    
    private Vector3 _siz;

    private Transform _shoot;
    
    // Start is called before the first frame update
    void Start()
    {
        _shoot = transform;

        // get the size of the Sprite
        _siz.x = gameObject.GetComponent<SpriteRenderer>().bounds.size.x;
        _siz.y = gameObject.GetComponent<SpriteRenderer>().bounds.size.y;

        
        _movement = new Vector2(-5.0f, 0f);

        GetComponent<Rigidbody2D>().velocity = _movement;
    }

    // Update is called once per frame
    void Update()
    {
        // delete when out of window
        if (_shoot.position.x < GameState.Instance.LeftBottomCameraBorder.x - _siz.x / 2)
        {
            Destroy(gameObject);
        }
    }
}
