﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameState : MonoBehaviour
{
    private static GameState _instance = null;

    // Cameras
    private Vector3 _leftTopCameraBorder;
    private Vector3 _rightTopCameraBorder;
    private Vector3 _leftBottomCameraBorder;
    private Vector3 _rightBottomCameraBorder;

    // display
    private int _scorePlayer;
    
    // shoot data
    private float shootX = 5.0f;
    private float shootY = 0.0f;
    private float shootCD = 1.0f;
    private float tripleShootCD = 2.0f;
    private float tripleShootCDCAP = 0.02f;
    private float EnemyShootCD = 2.0f;
    
    private float randPowerup = 10.0f;

    

    public static GameState Instance {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<GameState>();
                if (_instance == null)
                {
                    GameObject go = new GameObject();
                    go.name = "GameState";
                    _instance = go.AddComponent<GameState>();
                    // there is no dontdestroyOnLoad because it only used in the game not outside
                }
            }

            return _instance;
        }
    }

    public void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
            _scorePlayer = 0;
            if (!(Camera.main is null))
            {
                _leftBottomCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, 0));
                _rightBottomCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, 0));
                _leftTopCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(0, 1, 0));
                _rightTopCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(1, 1, 0));
            }
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public Vector3 LeftTopCameraBorder
    {
        get => _leftTopCameraBorder;
        set => _leftTopCameraBorder = value;
    }

    public Vector3 RightTopCameraBorder
    {
        get => _rightTopCameraBorder;
        set => _rightTopCameraBorder = value;
    }

    public Vector3 LeftBottomCameraBorder
    {
        get => _leftBottomCameraBorder;
        set => _leftBottomCameraBorder = value;
    }

    public Vector3 RightBottomCameraBorder
    {
        get => _rightBottomCameraBorder;
        set => _rightBottomCameraBorder = value;
    }

    public int ScorePlayer
    {
        get => _scorePlayer;
        set => _scorePlayer = value;
    }

    public float ShootX1
    {
        get => shootX;
        set => shootX = value;
    }

    public float ShootY1
    {
        get => shootY;
        set => shootY = value;
    }

    public float ShootCd1
    {
        get => shootCD;
        set => shootCD = value;
    }

    public float TripleShootCd1
    {
        get => Math.Max(tripleShootCD, tripleShootCDCAP);
        set => tripleShootCD = value;
    }

    
    public float RandPowerup
    {
        get => randPowerup;
        set => randPowerup = value;
    }

    public float EnemyShootCd
    {
        get => EnemyShootCD;
        set => EnemyShootCD = value;
    }

    // Update is called once per frame
    void Update()
    {
        GameObject.FindWithTag("scoreLabel").GetComponent<Text>().text = "" + _scorePlayer;
    }
}
