﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SocialPlatforms;

public class MovePowerUp : MonoBehaviour
{
    public Vector2 _movement;
    
    private Vector3 _siz;

    private Transform _powerup;
    
    // Start is called before the first frame update
    void Start()
    {
        _powerup = transform;

        // get the size of the Sprite
        _siz.x = gameObject.GetComponent<SpriteRenderer>().bounds.size.x;
        _siz.y = gameObject.GetComponent<SpriteRenderer>().bounds.size.y;

        
        _movement = new Vector2(Random.Range(-4f, -1.0f), Random.Range(-1f, 1f));

        GetComponent<Rigidbody2D>().velocity = _movement;
    }

    // Update is called once per frame
    void Update()
    {
        // delete when out of window
        if (_powerup.position.x > GameState.Instance.RightTopCameraBorder.x + _siz.x / 2)
        {
            Destroy(gameObject);
        }
    }
}
