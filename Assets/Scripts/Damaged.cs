﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Damaged : MonoBehaviour
{
    private int cpt = 0;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {    
        Color cl = GetComponent<SpriteRenderer>().color;
        if (cpt % 50 == 0)
            cl.a = (cl.a + 1) % 2;
        GetComponent<SpriteRenderer>().color = cl;
        if (cpt > 550)
            Destroy(this);
        cpt++;

    }
}
