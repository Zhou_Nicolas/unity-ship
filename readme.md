**Projet Unity Ship**

Ce projet est la suite du TD sur le Ship.
Il est en version 2020.1.8f1.
Le but du jeu est de détruire le plus d'ennemies afin de gagner le plus de points sans se faire toucher.
Pour cela, il y a des améliorations qui permettront d'aider à la destruction des ennemies.

La scène principale est le "Menu".

Voici les fonctionnalitées faites en td :
- déplacement du vaisseau  
- création d'astéroïdes   
- tire du vaisseau  
- collision entre tir / astéroïde et vaisseau / astéroïde  
- système de vie
- bruitages
- fade out
- score

Voici ce que j'ai intégré :
- ajout d'autres scènes (menu, jeu, fin de jeu, classement, instructions)
- classement permet de voir les cinq meilleurs scores
- mettre un nom au joueur
- animation du vaisseau
- temps de recharge sur les tirs
- un item power-up pour changer d'armes, lorsqu'on le prend plusieurs fois, cela réduit le temps de recharge
- d'autres tirs (triple tirs droits ou en angles)
- changer de tirs avec le bouton "x" lorsqu'on a reçu un power up 
- augmenter la difficulté lorsqu'on a beaucoup de points (augmenter le nombre d'ennemies, diminuer le temps de recharge)


De cela que je voulais faire, il reste plusieurs fonctionnalitées à intégrer :
- build android -> j'ai n'arrives pas à connecter avec un simulateur d'android
- modifier la vitesse des tirs -> Pas nécessaire en fin de compte comme il y a le temps de recharge
- mettre tous les textures dans un tileset -> pas le temps
- une liste pour changer de tirs -> au lieu d'avoir une juste un tire, pouvoir les stocker et changer comme on veut,
  cependant, je n'arrive pas à relier les classes de tirs avec le ship. 


Tout au long du projet, j'ai rencontré plusieurs problèmes.
J'avais eu un problème avec la factorisation du code afin de ne pas répéter les mêmes lignes de codes.
Lorsque je le mets dans une classe singleton, il n'arriverait pas à avoir les valeurs car il était initialisé avant.
Pour résoudre ce problème, il fallait initialiser les données dans la méthode Aware au lieu du Start. 
Ainsi, il est donc initialisé avant tous les autres (comme tous les autres sont lancer avec la méthode Start)  

J'avais eu une autre erreur avec l'audio pour le bgm. Il était compliqué d'avoir le son gardé sur tous les scènes. 
Pourtant, il fallait juste avoir un DontDestroyOnload, mais j'avais fait en sorte que seul le script ne se détruisait pas.
Il y a donc à chaque fois le gameobject qui est détruit et donc tous les données du script pour les sons ont disparu.
J'avais donc séparé le script pour le mettre dans un autre gameobject afin qu'il n'est plus de conflits avec l'ancient 
gameobject.

Le jeu est testé sur un Windows 10 et avec WebGL


Dépôt sur gitlab : https://gitlab.com/Zhou_Nicolas/unity-ship
